#ifndef SNAKE_HPP
#define SNAKE_HPP

#include "enum_snake.hpp"
#include <vector>
#include <set>

typedef unsigned short ushort;

class snake {
public:
	snake(const ushort HEIGHT_FIELD = 20, 
	      const ushort WIDTH_FIELD = 20);
	
	~snake() = default;
	
	STEP_SNAKE get_step() const;
	void set_step(STEP_SNAKE new_step);
	void step();

	bool end_game() const;
	bool wing_game() const;

	ushort size_height_field() const;
	ushort size_width_field() const; 

	const std::vector<std::vector<CELL_SNAKE> >& get_field() const;
	
	ushort size_snake() const;
	std::pair<ushort, ushort> head_snake() const;
	std::pair<ushort, ushort> tail_snake() const;
	
	const std::set<std::pair<ushort, ushort> >& get_apple() const;
	const std::set<std::pair<ushort, ushort> >& get_obstacle() const;
	
	bool get_cell_snake(const std::pair<ushort, ushort> INDEX) const;
	bool get_cell_value(const std::pair<ushort, ushort> INDEX,
		            const CELL_SNAKE value_cell) const;


protected:
	const ushort _HEIGHT_FIELD;
	const ushort _WIDTH_FIELD;
	const ushort _FREE_ELEMENTS;

	std::vector<std::pair<ushort, ushort> > _snake;
	std::vector<std::vector<CELL_SNAKE> > _field;

	std::set<std::pair<ushort, ushort> > _apple;
	std::set<std::pair<ushort, ushort> > _obstacle;
	
	STEP_SNAKE _step;
	bool _end_game;

	void _creat_field();
	void _filling_field();
	void _free_field();
	void _deletions_field();
	void _set_cell_field(const std::pair<ushort, ushort> INDEX, 
			     const CELL_SNAKE new_value_cell);
	
	std::pair<ushort, ushort> _generation_apple() const;
	std::pair<ushort, ushort> _new_head_snake_index() const;
};			
#endif
