#ifndef SNAKEQT_H
#define SNAKEQT_H

#include <QMainWindow>
#include <QTimer>
#include <QPaintEvent>

#include "snake/snake.hpp"

QT_BEGIN_NAMESPACE
namespace Ui {
    class snakeQt;
}
QT_END_NAMESPACE

class snakeQt : public QMainWindow
{
    Q_OBJECT

public:
    snakeQt(QWidget *parent = nullptr);
    ~snakeQt();

private slots:
    void on__new_exit_clicked();

    void on__exit_clicked();

    void on__game_clicked();

    void on__new_menu_clicked();

    void on__new_game_clicked();

protected:
    Ui::snakeQt* _ui;
    snake* _snake;
    QTimer* _timer;
    bool _game;
    int _keyEvent;

    void _hiding_menu();
    void _show_menu();
    void _hiding_game_element();
    void _show_game_element();
    void _move();
    void _signal_move();

    int _size_cell() const ;
    QPair<int, int> _begin_indent(int) const;


    void keyPressEvent(QKeyEvent* event);
    void paintEvent(QPaintEvent*);
    void resizeEvent(QResizeEvent*);

    void _print_field_snake();
    void _print_end_game();
};
#endif
