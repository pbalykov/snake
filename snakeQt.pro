QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

SOURCES += \
    scr/main.cpp \
    scr/snakeqt.cpp \
    scr/snake/snake.cpp

HEADERS += \
    headers/snakeqt.h \
    headers/snake/snake.hpp \
    headers/snake/enum_snake.hpp

FORMS += \
    forms/snakeqt.ui


OBJECTS_DIR = obj
DESTDIR = bin
