#include "headers/snake/snake.hpp"
#include <stdexcept>

void snake::_creat_field() {
	for (ushort i = 0; i < this->_HEIGHT_FIELD; i++) {
		this->_field.push_back(std::vector<CELL_SNAKE> (this->_WIDTH_FIELD,
							      CELL_SNAKE::NONE) ); 
	}
	return ;
}

void snake::_filling_field() {
	for (size_t i = 0; i < this->_snake.size(); i++) {
		if ( !i ) {
			this->_set_cell_field(this->_snake[i], CELL_SNAKE::HEAD_SNAKE);
		}
		else if ( i + 1 == this->_snake.size() ) {
			this->_set_cell_field(this->_snake[i], CELL_SNAKE::TAIL_SNAKE);
		}
		else {
			this->_set_cell_field(this->_snake[i], CELL_SNAKE::TORSOS_SNAKE);
		}
	}
	for (auto i = this->_apple.begin(); i != this->_apple.end(); i++) {
		this->_set_cell_field(*i, CELL_SNAKE::APPLE);
	}
	for (auto i = this->_obstacle.begin(); i != this->_obstacle.end(); i++) {
		this->_set_cell_field(*i, CELL_SNAKE::OBSTACLE);
	}
	return ;
}				      			

void snake::_free_field() {
	for (ushort i = 0; i < this->_field.size(); i++) {
		for (ushort j = 0; j < this->_field[i].size(); j++) {
			this->_set_cell_field(std::make_pair(i, j), 
					      CELL_SNAKE::NONE);
		}
	}
	return ;
}

void snake::_deletions_field() {
	for (ushort i = 0; i < this->_field.size(); i++) {
		this->_field[i].~vector();
	}
	this->_field.~vector();
	return ;
}

bool snake::get_cell_snake(const std::pair<ushort, ushort> INDEX) const {
	return this->get_cell_value(INDEX, CELL_SNAKE::HEAD_SNAKE) ||
	       this->get_cell_value(INDEX, CELL_SNAKE::TORSOS_SNAKE) ||
	       this->get_cell_value(INDEX, CELL_SNAKE::TAIL_SNAKE) ; 
}

bool snake::get_cell_value(const std::pair<ushort, ushort> INDEX,
	    		   const CELL_SNAKE value_cell) const {
	if ( INDEX.first >= this->_HEIGHT_FIELD || INDEX.second >= _WIDTH_FIELD ) {
		throw std::out_of_range("Error: going beyond the field size");
	}
	return this->_field[INDEX.first][INDEX.second] == value_cell;
}

void snake::_set_cell_field(const std::pair<ushort, ushort> INDEX,	    	      
    		            const CELL_SNAKE NEW_VALUE_CELL) {

	if ( INDEX.first >= this->_HEIGHT_FIELD || INDEX.second >= _WIDTH_FIELD ) {
		throw std::out_of_range("Error: going beyond the field size");
	}
	this->_field[INDEX.first][INDEX.second] = NEW_VALUE_CELL;
	return ;
}	

std::pair<ushort, ushort> snake::_generation_apple() const {
	std::vector<std::pair<ushort, ushort> > free_cell;
	for (ushort i = 0; i < this->_HEIGHT_FIELD; i++) {
		for (ushort j = 0; j < this->_WIDTH_FIELD; j++) {
			if ( this->_field[i][j] == CELL_SNAKE::NONE ) {
				free_cell.push_back(std::make_pair(i, j));
			}
		}
	}
	if ( free_cell.size() ) { 
		ushort index = rand() % free_cell.size();
		return free_cell[index];
	}
	return std::make_pair(this->_HEIGHT_FIELD, this->_WIDTH_FIELD);
}

std::pair<ushort, ushort> snake::_new_head_snake_index() const {
	std::pair<short, short> new_head_snake = this->_snake[0];
	switch ( this->_step ) {
		case STEP_SNAKE::RIGHT :
			new_head_snake.second += 1;
			break;

		case STEP_SNAKE::LEFT :
			new_head_snake.second -= 1;
			break;

		case STEP_SNAKE::DOWN : 
			new_head_snake.first += 1;
			break;

		case STEP_SNAKE::UP :
			new_head_snake.first -= 1;
			break;

		default:
			throw std::invalid_argument("Error: there is no such move!!!");
	}
	new_head_snake.first = new_head_snake.first % this->_HEIGHT_FIELD;
	new_head_snake.first += this->_HEIGHT_FIELD;
	new_head_snake.first %= this->_HEIGHT_FIELD;

	new_head_snake.second = new_head_snake.second % this->_WIDTH_FIELD; 
	new_head_snake.second += this->_WIDTH_FIELD;
	new_head_snake.second %= this->_WIDTH_FIELD;

	return new_head_snake;
}

snake::snake(const ushort HEIGHT_FIELD, const ushort WIDTH_FIELD) : 
		    _HEIGHT_FIELD(HEIGHT_FIELD), _WIDTH_FIELD(WIDTH_FIELD) ,
		    _FREE_ELEMENTS(HEIGHT_FIELD * WIDTH_FIELD) {

	if ( this->_HEIGHT_FIELD < 2 || this->_WIDTH_FIELD < 2 || 
	     this->_HEIGHT_FIELD > 10000 || this->_WIDTH_FIELD > 10000) {
		throw std::out_of_range("Error: fields can't be this size!!!");
	}
	this->_end_game = false;
	this->_step = STEP_SNAKE::RIGHT;
	this->_creat_field();
	
	std::pair<ushort, ushort> index = std::make_pair(0, 0);
	this->_snake.push_back(index);
	this->_set_cell_field(index, CELL_SNAKE::HEAD_SNAKE);

	index = this->_generation_apple();
	this->_apple.insert(index);
	this->_set_cell_field(index, CELL_SNAKE::APPLE);
	return ;
}

STEP_SNAKE snake::get_step() const {
	return this->_step;
}

void snake::set_step(STEP_SNAKE new_step) {
	if ( ( this->_step == STEP_SNAKE::LEFT && new_step == STEP_SNAKE::RIGHT ) || 
	     ( this->_step == STEP_SNAKE::RIGHT && new_step == STEP_SNAKE::LEFT ) ||
	     ( this->_step == STEP_SNAKE::UP && new_step == STEP_SNAKE::DOWN) ||
	     ( this->_step == STEP_SNAKE::DOWN && new_step == STEP_SNAKE::UP ) ) {
		return ;
	}
	this->_step = new_step;
	return ;
}

void snake::step() {
	std::pair<ushort, ushort> new_head_snake = this->_new_head_snake_index();
	this->_set_cell_field(*(this->_snake.end() - 1), CELL_SNAKE::NONE);

	if ( this->get_cell_snake(new_head_snake) ||
	     this->get_cell_value(new_head_snake, CELL_SNAKE::OBSTACLE) ) {
		this->_end_game = true;
		return ;
	}
	else if ( this->get_cell_value(new_head_snake, CELL_SNAKE::NONE) )  {
		this->_snake.erase(this->_snake.end() - 1);
	}
	else if (this->get_cell_value(new_head_snake, CELL_SNAKE::APPLE ) ) {
		this->_apple.erase(new_head_snake);
		std::pair<ushort, ushort> new_apple = this->_generation_apple();
		if ( new_apple.first != this->_HEIGHT_FIELD ) {
			this->_set_cell_field(new_apple, CELL_SNAKE::APPLE);
			this->_apple.insert(new_apple);
		}
	}
	else {
		throw std::invalid_argument("Error: there is no such cell!!!");
	}	

	if ( this->_snake.size() ) {
		this->_set_cell_field(*this->_snake.begin(), CELL_SNAKE::TORSOS_SNAKE);
		this->_set_cell_field(*this->_snake.rbegin(), CELL_SNAKE::TAIL_SNAKE);
	}
	this->_set_cell_field(new_head_snake, CELL_SNAKE::HEAD_SNAKE);
	this->_snake.insert(this->_snake.begin(), new_head_snake);
	return ;
}

bool snake::end_game() const {
	return this->_end_game;
}

bool snake::wing_game() const {
	return this->_snake.size() == this->_FREE_ELEMENTS;
}

ushort snake::size_height_field() const {
	return this->_HEIGHT_FIELD;
}

ushort snake::size_width_field() const{ 
	return this->_WIDTH_FIELD;
}

const std::vector<std::vector<CELL_SNAKE> >& snake::get_field() const {
       return this->_field;
}       

ushort snake::size_snake() const { 
	return this->_snake.size();
}

std::pair<ushort, ushort> snake::head_snake() const {
	return this->_snake[0];
}

std::pair<ushort, ushort> snake::tail_snake() const {
	return this->_snake[this->_snake.size() - 1];
}

const std::set<std::pair<ushort, ushort> >& snake::get_apple() const {
	return this->_apple;
}

const std::set<std::pair<ushort, ushort> >& snake::get_obstacle() const {
	return this->_obstacle;
}
