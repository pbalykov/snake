#include <QApplication>
#include <cstdlib>
#include <ctime>
#include "headers/snakeqt.h"


int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    QApplication draw(argc, argv);
    snakeQt root;
    root.setWindowTitle("Snake");
    root.show();
    return draw.exec();
}
