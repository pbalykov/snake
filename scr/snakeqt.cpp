#include "headers/snakeqt.h"
#include "ui_snakeqt.h"

#include <QKeyEvent>
#include <QPainter>
#include <iostream>
#include <QMessageBox>

snakeQt::snakeQt(QWidget *parent) : QMainWindow(parent), _ui(new Ui::snakeQt),
                                    _game(false), _keyEvent(0)
{
    this->_ui->setupUi(this);
    this->_snake = nullptr;
    this->_timer = nullptr;
    this->_hiding_game_element();
}

snakeQt::~snakeQt()
{
    delete this->_ui;
    delete this->_snake;
    delete this->_timer;
    this->_ui = nullptr;
    this->_snake = nullptr;
    this->_timer = nullptr;
    this->_game = false;
    return ;
}


void snakeQt::on__new_exit_clicked()
{
    this->close();
    return ;
}


void snakeQt::on__exit_clicked()
{
    this->close();
    return ;
}


void snakeQt::_hiding_menu()
{
    this->_ui->_exit->hide();
    this->_ui->_game->hide();
    this->_ui->_about_game->hide();
    return ;
}

void snakeQt::_show_menu()
{
    this->_ui->_exit->show();
    this->_ui->_game->show();
    this->_ui->_about_game->show();
    return ;
}

void snakeQt::_hiding_game_element()
{
    this->_ui->_new_exit->hide();
    this->_ui->_new_game->hide();
    this->_ui->_new_menu->hide();
    return ;
}

void snakeQt::_show_game_element()
{
    this->_ui->_new_exit->show();
    this->_ui->_new_game->show();
    this->_ui->_new_menu->show();
    return ;
}

void snakeQt::_move()
{
    switch ( this->_keyEvent )
    {
        case Qt::Key_D :
            this->_snake->set_step(STEP_SNAKE::RIGHT);
            break ;

        case Qt::Key_A :
            this->_snake->set_step(STEP_SNAKE::LEFT);
            break ;

        case Qt::Key_W :
            this->_snake->set_step(STEP_SNAKE::UP);
            break ;

        case Qt::Key_S :
            this->_snake->set_step(STEP_SNAKE::DOWN);
            break ;
    }
    return ;
}

void snakeQt::keyPressEvent(QKeyEvent* event)
{
    if ( !this->_game && !this->_snake->end_game() ) {
        return ;
    }
    this->_keyEvent = event->key();
    return ;
}

void snakeQt::paintEvent(QPaintEvent* )
{
    if ( !this->_game ) {
        return ;
    }
    this->_print_field_snake();
}

void snakeQt::resizeEvent(QResizeEvent *)
{
    this->repaint();
    return ;
}

void snakeQt::_print_field_snake()
{
    int size_cell = this->_size_cell();
    QPair<int, int> begin_indent = this->_begin_indent(size_cell);
    const auto field_snake = this->_snake->get_field();
    QPainter painter(this);
    for (size_t i = 0; i < field_snake.size(); i++) {
        for (size_t j = 0; j < field_snake[i].size(); j++) {
		if ( this->_snake->get_cell_snake(std::make_pair(i, j)) ) {
                    painter.setBrush(QBrush(Qt::green, Qt::SolidPattern));
                }

		else if ( this->_snake->get_cell_value({i, j}, CELL_SNAKE::APPLE) ) {
                    painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
                }

		else if ( this->_snake->get_cell_value({i, j}, CELL_SNAKE::OBSTACLE ) ) {
                    painter.setBrush(QBrush(Qt::darkCyan, Qt::SolidPattern));
                }
                else {
                    painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
                }
           	painter.drawRect(begin_indent.first + j * size_cell, begin_indent.second + i * size_cell,
               	                 size_cell, size_cell);
        }
    }
    return;
}

void snakeQt::on__game_clicked()
{
    this->_hiding_menu();
    this->_show_game_element();
    this->_snake = new snake();
    this->_game = true;
    this->repaint();
    this->_timer = new QTimer(this);
    this->connect(this->_timer, &QTimer::timeout, [&]{this->_signal_move();});
    this->_timer->start(200);
    return ;
}


void snakeQt::on__new_menu_clicked()
{
    this->_timer->stop();
    delete this->_timer;
    delete this->_snake;
    this->_timer = nullptr;
    this->_snake = nullptr;
    this->_game = false;
    this->_hiding_game_element();
    this->_show_menu();
    this->repaint();
    return ;
}

void snakeQt::_signal_move()
{
    this->_move();
    if ( !this->_snake->end_game() ) {
        this->_snake->step();
    }
    else {
        this->_timer->stop();


        QMessageBox msg = QMessageBox();
        msg.setWindowFlags(Qt::FramelessWindowHint);
        msg.setWindowTitle("End game");
        msg.setText("Увы но вы проиграли");
        msg.exec();
    }
    this->repaint();
    return ;
}

int snakeQt::_size_cell() const
{
    int max_cell_x = this->size().width() / this->_snake->size_width_field();
    int max_cell_y = this->_ui->_name_game->size().height() * 2 / 1.5 + this->_ui->_name_game->pos().y();
    max_cell_y += this->size().height() - this->_ui->_new_exit->pos().y();
    max_cell_y = (this->size().height() - max_cell_y) / this->_snake->size_width_field();
    return qMin(max_cell_y, max_cell_x);
}

QPair<int, int> snakeQt::_begin_indent(int size_cell) const
{
    int x_begin = this->size().width()  / 2 - size_cell * this->_snake->size_width_field() / 2;
    int y_begin = this->_ui->_new_exit->pos().y();
    y_begin -= this->_ui->_name_game->size().height() * 2 / 1.5 + this->_ui->_name_game->pos().y();
    y_begin = y_begin / 2 - size_cell * this->_snake->size_width_field() / 2 + this->_ui->_name_game->size().height() * 2 / 1.5;
    return {x_begin, y_begin};
}


void snakeQt::on__new_game_clicked()
{
    this->on__new_menu_clicked();
    this->on__game_clicked();
    return ;
}

